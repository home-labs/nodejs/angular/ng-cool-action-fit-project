import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { NgCoolactionFitPipeModule } from '@cyberjs.on/ng-coolaction-fit-pipe';


type Collection = {

    name?: string;

    gender?: string;

}


@Component({
    selector: 'app-root',
    standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        NgCoolactionFitPipeModule
    ],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {

    objectCollection: Collection[];

    filterTerm: string;

    constructor() {
        this.objectCollection = [
            {
                name: 'Rafael Pereira Laurindo Bastos',
                gender: 'masculino'
            },
            {
                name: 'Rafael Pereira Rafa',
                gender: 'masculino'
            },
            {
                name: 'Roberto Carlos Gomes Laurindo',
                gender: 'masculino'
            },
            {
                name: 'Delano Emanoel Bastos',
                gender: 'masculino'
            },
            {
                name: 'Sandra Suely Candido Bastos',
                gender: 'feminino'
            },
            {
                name: 'Joelma Candido Bastos Laurindo',
                gender: 'feminino'
            }
        ];

        this.filterTerm = '';
    }

}
