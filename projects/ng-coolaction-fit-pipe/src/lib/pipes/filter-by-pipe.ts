import {
    Pipe,
    PipeTransform
} from '@angular/core';

import { CoolactionFit } from '@cyberjs.on/coolaction-fit';


@Pipe({
    name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

    private filter: CoolactionFit.Filter;

    constructor() {
        this.filter = new CoolactionFit.Filter();
    }

    transform<T extends Object>(collection: T[], term: string, ...properties: string[]): T[] {

        const filtered: T[] = [];

        // cause' of asynchronous loading
        if (!collection || !collection.length) {
            return [];
        }

        // do not stop to use the resolveData method if the term is empty, otherwise the cache will never be emptied, which may create unexpected results
        this.filter.resolveData(collection, term, ...properties).forEach(
            (map: CoolactionFit.FilteredData) => {
                filtered.push(map.source as T);
            }
        );

        return filtered;
    }

}
